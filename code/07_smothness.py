# %%
import cv2 as cv
import matplotlib.pyplot as plt
import os
import numpy as np

cur_dir = os.path.dirname(os.path.abspath(__file__))
img_dir = os.path.abspath(os.path.join(
    cur_dir, os.pardir, 'materials/unzip/02.code/image'))
print(img_dir)

# %%
dogsp = cv.imread(os.path.join(img_dir, 'dogsp.jpeg'))
doggau = cv.imread(os.path.join(img_dir, 'dogGauss.jpeg'))

# %% [markdown]
# # 均值滤波
# %%
dog = cv.blur(dogsp, (5, 5))
plt.imshow(dog[:, :, ::-1])

# %% [markdown]
# # 高斯滤波

# %%
blur = cv.GaussianBlur(doggau, (3, 3), 1)
plt.figure(figsize=(10, 8), dpi=100)
plt.subplot(121)
plt.imshow(doggau[:, :, ::-1])
plt.title('origin')
plt.xticks([])
plt.yticks([])
plt.subplot(122)
plt.imshow(blur[:, :, ::-1])
plt.title('gaussian')
plt.xticks([])
plt.yticks([])

# %% [markdown]
# # 中值滤波

# %%
dog = cv.medianBlur(dogsp, 3)
plt.imshow(dog[:, :, ::-1])

# %%
