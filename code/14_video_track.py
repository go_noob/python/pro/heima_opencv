import cv2 as cv
from matplotlib import cm
import matplotlib.pyplot as plt
import os
import numpy as np

cur_dir = os.path.dirname(os.path.abspath(__file__))
img_dir = os.path.abspath(os.path.join(
    cur_dir, os.pardir, 'materials/unzip/02.code/image'))
print(img_dir)

cap = cv.VideoCapture(os.path.join(img_dir, 'DOG.wmv'))
# 指定追踪目标
ret, frame = cap.read()
r, h, c, w = 197, 141, 0, 208
win = (c, r, w, h)
roi = frame[r:r+h, c:c+w]
# 计算直方图
hsv_roi = cv.cvtColor(roi, cv.COLOR_BGR2HSV)
roi_hist = cv.calcHist([hsv_roi], [0], None, [180], [0, 180])
cv.normalize(roi_hist, 0, 255, cv.NORM_MINMAX)
# 目标追踪
term = (cv.TERM_CRITERIA_EPS | cv.TERM_CRITERIA_COUNT, 10, 1)
while True:
    ret, frame = cap.read()
    if ret:
        hsv = cv.cvtColor(frame, cv.COLOR_BGR2HSV)
        dst = cv.calcBackProject([hsv], [0], roi_hist, [0, 180], 1)
        ret, win = cv.meanShift(dst, win, term)
        x, y, w, h = win
        img2 = cv.rectangle(frame, (x, y), (x+w, y+h), 255, 2)
        cv.imshow('frame', img2)
        if cv.waitKey(60) & 0xff == ord('q'):
            break
    else:
        break
# 释放资源
cap.release()
cv.destroyAllWindows()
