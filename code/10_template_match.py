# %%
import cv2 as cv
from matplotlib import cm
import matplotlib.pyplot as plt
import os
import numpy as np

cur_dir = os.path.dirname(os.path.abspath(__file__))
img_dir = os.path.abspath(os.path.join(
    cur_dir, os.pardir, 'materials/unzip/02.code/image'))
print(img_dir)

# %%
img = cv.imread(os.path.join(img_dir, 'wulin.jpeg'))
plt.imshow(img[:, :, ::-1])

# %%
template = cv.imread(os.path.join(img_dir, 'bai.jpeg'))
plt.imshow(template[:, :, ::-1])

# %%
res = cv.matchTemplate(img, template, cv.TM_CCORR)
plt.imshow(res, cmap=plt.cm.gray)
# %%
min_val, max_val, min_loc, max_loc = cv.minMaxLoc(res)
top_left = max_loc
h, w = template.shape[:2]
bottom_right = (top_left[0]+w, top_left[1]+h)
cv.rectangle(img, top_left, bottom_right, (0, 255, 0), 2)
plt.imshow(img[:, :, ::-1])

# %%
