# %%
import cv2 as cv
import matplotlib.pyplot as plt
import os
from pathlib import Path
import numpy as np

cur_dir = os.path.dirname(os.path.abspath(__file__))
img_dir = os.path.abspath(os.path.join(cur_dir, os.pardir, 'materials/unzip/02.code/image'))
print(img_dir)

# %%
img = np.zeros((256, 256, 3), np.uint8)
plt.imshow(img[:, :, ::-1])


# %%
img[100, 100]
# %%
img[100, 100, 0]

# %%
img[100, 100] = (0, 0, 255)
# %%
plt.imshow(img[:, :, ::-1])
# %%
img.shape
# %%
img.dtype
# %%
img.size
# %%
dili = cv.imread(os.path.join(img_dir, 'dili.jpg'))
plt.imshow(dili[:, :, ::-1])
# %%
# 通道拆分
b, g, r = cv.split(dili)
plt.imshow(b, cmap=plt.cm.gray)
# %%
# 通道合并
img2 = cv.merge((b, g, r))
plt.imshow(img2[:, :, ::-1])
# %%
# 色彩空间的改变
gray = cv.cvtColor(dili, cv.COLOR_BGR2GRAY)
plt.imshow(gray, cmap=plt.cm.gray)
hsv = cv.cvtColor(dili, cv.COLOR_BGR2HSV)
plt.imshow(hsv)

# %%
