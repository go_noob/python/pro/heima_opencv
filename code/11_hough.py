# %%
import cv2 as cv
from matplotlib import cm
import matplotlib.pyplot as plt
import os
import numpy as np

cur_dir = os.path.dirname(os.path.abspath(__file__))
img_dir = os.path.abspath(os.path.join(
    cur_dir, os.pardir, 'materials/unzip/02.code/image'))
print(img_dir)
# %% [markdown]
# # 线检测
# %%
img = cv.imread(os.path.join(img_dir, 'rili.jpg'))
edges = cv.Canny(img, 50, 150)
plt.imshow(edges, cmap=plt.cm.gray)
# %%
lines = cv.HoughLines(edges, 0.8, np.pi/180, 150)
for line in lines:
    rho, theta = line[0]
    a = np.cos(theta)
    b = np.sin(theta)
    x0 = rho*a
    y0 = rho*b
    x1 = int(x0+1000*(-b))
    y1 = int(y0+1000*a)
    x2 = int(x0-1000*(-b))
    y2 = int(y0-1000*a)
    cv.line(img, (x1, y1), (x2, y2), (0, 255, 0))
plt.imshow(img[:, :, ::-1])
# %% [markdown]
# # 圆检测
# %%
star = cv.imread(os.path.join(img_dir, 'star.jpeg'))
gray_img = cv.cvtColor(star, cv.COLOR_BGR2GRAY)
img = cv.medianBlur(gray_img, 7)
plt.imshow(img, cmap=plt.cm.gray)

# %%
circles = cv.HoughCircles(img, cv.HOUGH_GRADIENT, 1, 200, param1=100, param2=50, minRadius=0, maxRadius=100)
for i in circles[0, :]:
    cv.circle(star, (int(i[0]), int(i[1])), int(i[2]), (0, 255, 0), 2)
    cv.circle(star, (int(i[0]), int(i[1])), 2, (0, 255, 0), -1)
plt.imshow(star[:, :, ::-1])

# %%
