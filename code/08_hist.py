# %%
import cv2 as cv
from matplotlib import cm
import matplotlib.pyplot as plt
import os
import numpy as np

cur_dir = os.path.dirname(os.path.abspath(__file__))
img_dir = os.path.abspath(os.path.join(
    cur_dir, os.pardir, 'materials/unzip/02.code/image'))
print(img_dir)
# %%
img = cv.imread(os.path.join(img_dir, 'cat.jpeg'), 0)
plt.imshow(img, cmap=plt.cm.gray)
# %% [markdown]
# # 直方图
# %%
hist = cv.calcHist([img], [0], None, [256], [0, 256])
plt.figure(figsize=(10, 8))
plt.plot(hist)
plt.show()

# %% [markdown]
# # 掩膜的应用
# %%
plt.imshow(img, cmap=plt.cm.gray)

# %%
# 创建掩膜
mask = np.zeros(img.shape[:2], np.uint8)
mask[400:650, 200:500] = 1
plt.imshow(mask, cmap=plt.cm.gray)
# %%
mask_img = cv.bitwise_and(img, img, mask=mask)
plt.imshow(mask_img, cmap=plt.cm.gray)

# %%
mask_hist = cv.calcHist([img], [0], mask, [256], [0, 256])
plt.plot(mask_hist)
plt.show()
# %% [markdown]
# # 直方图均衡化

# %%
dst = cv.equalizeHist(img)
fig, axes = plt.subplots(2, 2, figsize=(10, 8), dpi=100)
axes[0, 0].imshow(img, cmap=plt.cm.gray)
axes[0, 0].set_title('origin')
axes[0, 1].imshow(dst, cmap=plt.cm.gray)
axes[0, 1].set_title('equalize hist')
axes[1, 0].plot(hist)
equal_hist = cv.calcHist([dst], [0], None, [256], [0, 256])
axes[1, 1].plot(equal_hist)
plt.show()

# %% [markdown]
# # 自适应均衡化

# %%
# 创建一个自适应均衡化的对象,并应用于图像
clahe = cv.createCLAHE(clipLimit=2.0, tileGridSize=(8, 8))
cl1 = clahe.apply(img)
fig, axes = plt.subplots(nrows=1, ncols=2, figsize=(10, 8), dpi=100)
axes[0].imshow(img, cmap=plt.cm.gray)
axes[0].set_title('origin')
axes[1].imshow(cl1, cmap=plt.cm.gray)
axes[1].set_title('adaptive histogram equalization')
plt.show()
# %%
