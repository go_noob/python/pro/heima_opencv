# %%
import cv2 as cv
import matplotlib.pyplot as plt
import os
import numpy as np

cur_dir = os.path.dirname(os.path.abspath(__file__))
img_dir = os.path.abspath(os.path.join(cur_dir, os.pardir, 'materials/unzip/02.code/image'))
print(img_dir)

# %% [markdown]
# # 腐蚀与膨胀
# %%
img = cv.imread(os.path.join(img_dir, 'letter.png'))
plt.imshow(img[:, :, ::-1])

# %%
# 创建核结构
kenel = np.ones((5, 5), np.uint8)

# %%
# 腐蚀
img2 = cv.erode(img, kenel)
plt.imshow(img2[:, :, ::-1])

# %%
# 膨胀
img3 = cv.dilate(img, kenel)
plt.imshow(img3[:, :, ::-1])

# %% [markdown]
# # 开闭运算

# %%
open_img = cv.imread(os.path.join(img_dir, 'letteropen.png'))
plt.imshow(open_img[:, :, ::-1])

# %%
kenel = np.ones((10, 10), np.uint8)
cvopen = cv.morphologyEx(open_img, cv.MORPH_OPEN, kenel)
plt.imshow(cvopen[:, :, ::-1])

# %%
close_img = cv.imread(os.path.join(img_dir, 'letterclose.png'))
plt.imshow(close_img[:, :, ::-1])


# %%
cvclose = cv.morphologyEx(close_img, cv.MORPH_CLOSE, kenel)
plt.imshow(cvclose[:, :, ::-1])
# %% [markdown]
# # 黑帽和礼帽

# %%
top = cv.morphologyEx(open_img, cv.MORPH_TOPHAT, kenel)
plt.imshow(top[:, :, ::-1])

# %%
black = cv.morphologyEx(close_img, cv.MORPH_BLACKHAT,kenel)
plt.imshow(black[:, :, ::-1])


# %%
