# %%
import cv2 as cv
from matplotlib import cm
import matplotlib.pyplot as plt
import os
import numpy as np

cur_dir = os.path.dirname(os.path.abspath(__file__))
img_dir = os.path.abspath(os.path.join(
    cur_dir, os.pardir, 'materials/unzip/02.code/image'))
print(img_dir)

print(cv.__file__)

# %%
img = cv.imread(os.path.join(img_dir, 'yangzi.jpg'))
gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
# 实例化检测器
face_file = os.path.join(img_dir, os.pardir, 'haarcascade_frontalface_default.xml')
eye_file = os.path.join(img_dir, os.pardir, 'haarcascade_eye.xml')
face_cas = cv.CascadeClassifier(face_file)
face_cas.load(face_file)

eyes_cas = cv.CascadeClassifier(eye_file)
eyes_cas.load(eye_file)
# %%
# 人脸检测
face_rects = face_cas.detectMultiScale(gray, scaleFactor=1.2, minNeighbors=3, minSize=(32, 32))
# 绘制人脸检眼睛
for facerect in face_rects:
    x, y, w, h = facerect
    cv.rectangle(img, (x, y), (x+w, y+h), (0, 255, 0), 3)
    roi_color = img[y:y+h, x:x+w]
    roi_gray = gray[y:y+h, x:x+w]
    eyes = eyes_cas.detectMultiScale(roi_gray)
    for (ex, ey, ew, eh) in eyes:
        cv.rectangle(roi_color, (ex, ey), (ex+ew, ey+eh), (0, 255, 0), 3)
plt.imshow(img[:, :, ::-1])

# %%
