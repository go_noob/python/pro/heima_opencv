# %%
import cv2 as cv
from matplotlib import cm
import matplotlib.pyplot as plt
import os
import numpy as np

cur_dir = os.path.dirname(os.path.abspath(__file__))
img_dir = os.path.abspath(os.path.join(
    cur_dir, os.pardir, 'materials/unzip/02.code/image'))
print(img_dir)
# %%
img = cv.imread(os.path.join(img_dir, 'chessboard.jpg'))
gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
gray = np.float32(gray)

# %% [markdown]
# # Harris
# %%
plt.figure(figsize=(10, 10))
dst = cv.cornerHarris(gray, 2, 3, 0.04)
img[dst > 0.001*dst.max()] = [0, 0, 255]
plt.imshow(img[:, :, ::-1])
# %% [markdown]
# # shi-Tomas
# %%
img = cv.imread(os.path.join(img_dir, 'tv.jpg'))
gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
coners = cv.goodFeaturesToTrack(gray, 1000, 0.01, 10)
for i in coners:
    x, y = i.ravel()
    cv.circle(img, (int(x), int(y)), 2, (0, 0, 255), -1)
plt.imshow(img[:, :, ::-1])

# %% [markdown]
# # SIFT
# %%
img = cv.imread(os.path.join(img_dir, 'tv.jpg'))
gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
sift = cv.xfeatures2d.SIFT_create()
kp, des = sift.detectAndCompute(gray, None)
cv.drawKeypoints(img, kp, img, flags=cv.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
plt.imshow(img[:, :, ::-1])
# %% [markdown]
# # FAST

# %%
img = cv.imread(os.path.join(img_dir, 'tv.jpg'))
fast = cv.FastFeatureDetector_create(threshold=30)
kp = fast.detect(img, None)
img2 = cv.drawKeypoints(img, kp, None, color=(0, 0, 255))
plt.imshow(img2[:, :, ::-1])

# %%
fast.setNonmaxSuppression(0)
kp = fast.detect(img, None)
img3 = cv.drawKeypoints(img, kp, None, color=(0, 0, 255))
plt.imshow(img3[:, :, ::-1])
# %% [markdown]
# # orb
# %%
img = cv.imread(os.path.join(img_dir, 'tv.jpg'))
orb = cv.ORB_create(nfeatures=5000)
kp, des = orb.detectAndCompute(img, None)
img2 = cv.drawKeypoints(img, kp, None, flags=0)
plt.imshow(img2[:, :, ::-1])

# %%
