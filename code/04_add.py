# %%
import cv2 as cv
import matplotlib.pyplot as plt
import os
from pathlib import Path
import numpy as np

cur_dir = os.path.dirname(os.path.abspath(__file__))
img_dir = os.path.abspath(os.path.join(cur_dir, os.pardir, 'materials/unzip/02.code/image'))
print(img_dir)

# %%
rain = cv.imread(os.path.join(img_dir, 'rain.jpg'))
plt.imshow(rain[:, :, ::-1])
# %%
view = cv.imread(os.path.join(img_dir, 'view.jpg'))
plt.imshow(view[:, :, ::-1])
# %%
img1 = cv.add(rain, view)
plt.imshow(img1[:, :, ::-1])

# %%
img2 = rain+view
plt.imshow(img2[:, :, ::-1])

# %%
# 带权重和偏移的混合
img3 = cv.addWeighted(view, 0.7, rain, 0.3, 0)
plt.imshow(img3[:, :, ::-1])

# %%
