# %%
import cv2 as cv
from matplotlib import cm
import matplotlib.pyplot as plt
import os
import numpy as np

cur_dir = os.path.dirname(os.path.abspath(__file__))
img_dir = os.path.abspath(os.path.join(
    cur_dir, os.pardir, 'materials/unzip/02.code/image'))
print(img_dir)

# %%
img = cv.imread(os.path.join(img_dir, 'horse.jpg'), 0)
plt.imshow(img, cmap=plt.cm.gray)
# %% [markdown]
# # Sobel
# %%
x = cv.Sobel(img, cv.CV_16S, 1, 0)
y = cv.Sobel(img, cv.CV_16S, 0, 1)
absx = cv.convertScaleAbs(x)
absy = cv.convertScaleAbs(y)
res = cv.addWeighted(absx, 0.5, absy, 0.5, 0)
plt.imshow(res, cmap=plt.cm.gray)

# %% [markdown]
# # Schaar

# %%
x = cv.Sobel(img, cv.CV_16S, 1, 0, ksize=-1)
y = cv.Sobel(img, cv.CV_16S, 0, 1, ksize=-1)
absx = cv.convertScaleAbs(x)
absy = cv.convertScaleAbs(y)
schaar_img = cv.addWeighted(absx, 0.5, absy, 0.5, 0)
plt.imshow(schaar_img, cmap=plt.cm.gray)
# %% [markdown]
# # Laplacia

# %%
res = cv.Laplacian(img,cv.CV_16S)
res = cv.convertScaleAbs(res)
plt.imshow(res, cmap=plt.cm.gray)
# %% [markdown]
# # Canny

# %%
res = cv.Canny(img,0,100)
plt.imshow(res,cmap=plt.cm.gray)
# %%
