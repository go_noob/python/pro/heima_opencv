# %%
import cv2 as cv
from matplotlib import cm
import matplotlib.pyplot as plt
import os
import numpy as np

cur_dir = os.path.dirname(os.path.abspath(__file__))
img_dir = os.path.abspath(os.path.join(
    cur_dir, os.pardir, 'materials/unzip/02.code/image'))
print(img_dir)
temp_dir = os.path.abspath(os.path.join(cur_dir, os.pardir, 'temp'))
# %%
# 读取视频
# cap = cv.VideoCapture(os.path.join(img_dir, 'DOG.wmv'))
# while cap.isOpened():
#     ret, frame = cap.read()
#     if ret:
#         cv.imshow('frame', frame)
#     if cv.waitKey(25) & 0xff == ord('q'):
#         break
# cap.release()
# cv.destroyAllWindows()

# %%
# 保存图像
cap = cv.VideoCapture(os.path.join(img_dir, 'DOG.wmv'))
width = int(cap.get(3))
height = int(cap.get(cv.CAP_PROP_FRAME_HEIGHT))
out = cv.VideoWriter(os.path.join(temp_dir,'out.avi'),cv.VideoWriter_fourcc('M','J','P','G'),10,(width,height))
print(cap.isOpened())
while cap.isOpened():
    ret,frame=cap.read()
    if ret:
        out.write(frame)
    else:
        print(ret)
        break
cap.release()
out.release()
cv.destroyAllWindows()

# %%
