# %%
import cv2 as cv
import matplotlib.pyplot as plt
import os
from pathlib import Path
import numpy as np

cur_dir = os.path.dirname(os.path.abspath(__file__))
img_dir = os.path.abspath(os.path.join(cur_dir, os.pardir, 'materials/unzip/02.code/image'))
print(img_dir)

# %%
kids = cv.imread(os.path.join(img_dir, 'kids.jpg'))
plt.imshow(kids[:, :, ::-1])

# %%
# 绝对尺寸
rows, cols = kids.shape[:2]
# 注意: 第二个参数是 (列, 行)
res = cv.resize(kids, (2*cols, 2*rows))
plt.imshow(res[:, :, ::-1])

# %%
# 相对尺寸
res1 = cv.resize(kids, None, fx=0.5, fy=0.5)
plt.imshow(res1[:, :, ::-1])

# %% [markdown]
# # 图像平移
# %%
plt.imshow(kids[:, :, ::-1])
# %%
translation_mat = np.float32([[1, 0, 100], [0, 1, 50]])
# %%
res2 = cv.warpAffine(kids, translation_mat, (2*cols, 2*rows))
plt.imshow(res2[:, :, ::-1])

# %% [markdown]
# # 图像旋转
# %%
affine_mat = cv.getRotationMatrix2D((cols/2, rows/2), 45, 0.5)
res3 = cv.warpAffine(kids, affine_mat, (cols, rows))
plt.imshow(res3[:, :, ::-1])

# %% [markdown]
# # 仿射变换

# %%
# 三个点在原始图像上的位置
pts1 = np.float32([[50, 50], [200, 50], [50, 200]])
# 三个点在变换后的位置
pts2 = np.float32([[100, 100], [200, 50], [100, 250]])
affine_mat2 = cv.getAffineTransform(pts1, pts2)
res4 = cv.warpAffine(kids, affine_mat2, (cols, rows))
plt.imshow(res4[:, :, ::-1])

# %% [markdown]
# # 透射变换

# %%
# 投射前的四个点
pst1 = np.float32([[56, 65], [368, 52], [28, 387], [389, 390]])
pst2 = np.float32([[100, 145], [300, 100], [80, 290], [310, 300]])
per_mat = cv.getPerspectiveTransform(pst1, pst2)
res5 = cv.warpPerspective(kids, per_mat, (cols, rows))
plt.imshow(res5[:, :, ::-1])

# %% [markdown]
# # 图像金字塔
# %%
plt.imshow(kids[:, :, ::-1])
# %%
imgup = cv.pyrUp(kids)
plt.imshow(imgup[:, :, ::-1])

# %%
imgdown = cv.pyrDown(kids)
plt.imshow(imgdown[:, :, ::-1])

# %%
