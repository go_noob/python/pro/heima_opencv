import cv2 as cv
import matplotlib.pyplot as plt
import os
from pathlib import Path

cur_dir = os.path.dirname(os.path.abspath(__file__))
img_dir = os.path.abspath(os.path.join(cur_dir, os.pardir, 'image'))
print(img_dir)


# 1 读取图像
# img = cv.imread('./image/imori.jpg')
img = cv.imread(img_dir+'/imori.jpg', 0)

# 2 显示图像
# 2.1 opencv
# cv.imshow('myImg', img)
# cv.waitKey(0)
# cv.destroyAllWindows()

# 2.2 matplotlib
# plt.imshow(img[:, :, ::-1])
plt.imshow(img, cmap=plt.cm.gray)
plt.show()

# 3 图像保存
result_dir = img_dir+'/temp'
Path(result_dir).mkdir(parents=True, exist_ok=True)
cv.imwrite(result_dir+'/result.jpg', img)
