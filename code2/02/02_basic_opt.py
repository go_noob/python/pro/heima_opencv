# %%
# -*- coding: utf-8 -*-
import cv2 as cv
from matplotlib import cm
import matplotlib.pyplot as plt
import os
import numpy as np

cur_dir = os.path.dirname(os.path.abspath(__file__))
img_dir = os.path.abspath(os.path.join(
    cur_dir, os.pardir, os.pardir, 'materials/opencv2/img_opt'))
print(img_dir)
# %%
cat = cv.imread(os.path.join(img_dir, 'cat.jpg'))
plt.imshow(cat[:, :, ::-1])

# %% [markdown]
# # 边界填充
#
# - BORDER_REPLICATE：复制法，也就是复制最边缘像素。
# - BORDER_REFLECT：反射法，对感兴趣的图像中的像素在两边进行复制例如：fedcba|# abcdefgh|hgfedcb   
# - BORDER_REFLECT_101：反射法，也就是以最边缘像素为轴，对称，gfedcb|abcdefgh|# gfedcba
# - BORDER_WRAP：外包装法cdefgh|abcdefgh|abcdefg  
# - BORDER_CONSTANT：常量法，常数值填充。

# %%
top_size,bottom_size,left_size,right_size = (50,50,50,50)

replicate = cv.copyMakeBorder(cat, top_size, bottom_size, left_size, right_size, borderType=cv.BORDER_REPLICATE)
reflect = cv.copyMakeBorder(cat, top_size, bottom_size, left_size, right_size,cv.BORDER_REFLECT)
reflect101 = cv.copyMakeBorder(cat, top_size, bottom_size, left_size, right_size, cv.BORDER_REFLECT_101)
wrap = cv.copyMakeBorder(cat, top_size, bottom_size, left_size, right_size, cv.BORDER_WRAP)
constant = cv.copyMakeBorder(cat, top_size, bottom_size, left_size, right_size,cv.BORDER_CONSTANT, value=0)
plt.subplot(231), plt.imshow(cat, 'gray'), plt.title('ORIGINAL')
plt.subplot(232), plt.imshow(replicate, 'gray'), plt.title('REPLICATE')
plt.subplot(233), plt.imshow(reflect, 'gray'), plt.title('REFLECT')
plt.subplot(234), plt.imshow(reflect101, 'gray'), plt.title('REFLECT_101')
plt.subplot(235), plt.imshow(wrap, 'gray'), plt.title('WRAP')
plt.subplot(236), plt.imshow(constant, 'gray'), plt.title('CONSTANT')

plt.show()
# %% [markdown]
# # 傅里叶变换

# %%
img = cv.imread(os.path.join(img_dir, 'lena.jpg'), 0)
img_float32 = np.float32(img)

dft = cv.dft(img_float32, flags = cv.DFT_COMPLEX_OUTPUT)
dft_shift = np.fft.fftshift(dft)
# 得到灰度图能表示的形式
magnitude_spectrum = 20*np.log(cv.magnitude(dft_shift[:,:,0],dft_shift[:,:,1]))

plt.subplot(121),plt.imshow(img, cmap = 'gray')
plt.title('Input Image'), plt.xticks([]), plt.yticks([])
plt.subplot(122),plt.imshow(magnitude_spectrum, cmap = 'gray')
plt.title('Magnitude Spectrum'), plt.xticks([]), plt.yticks([])
plt.show()
# %%
